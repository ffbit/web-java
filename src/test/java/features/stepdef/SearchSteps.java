package features.stepdef;

import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import net.paoloambrosio.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import utils.page.SearchPage;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class SearchSteps {

    @Autowired
    private SearchPage searchPage;

    @Given("^I search for \"([^\"]*)\"$")
    public void iSearchFor(String query) {
        enterSomethingInTheSearchField(query);
        searchPage.submitSearch();
    }

    @Given("^I enter \"([^\"]*)\" in the search field$")
    public void enterSomethingInTheSearchField(String query) {
        searchPage.visit();
        searchPage.fillQueryField(query);
    }

    @Then("^the results should be:$")
    public void theResultsShouldBe(List<Message> expectedMessages) {
        List<String> searchResults = searchPage.searchResults();
        assertEquals(expectedMessages.size(), searchResults.size());
        for (int i = 0; i < searchResults.size(); ++i) {
            assertEquals(expectedMessages.get(i).getContent(), searchResults.get(i));
        }
    }

}
