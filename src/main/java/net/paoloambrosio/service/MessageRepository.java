package net.paoloambrosio.service;

import net.paoloambrosio.model.Message;

import java.util.List;

public interface MessageRepository {

    void save(Message message);

    List<Message> findAll();

    List<Message> findByAuthor(String username);

    List<Message> findByContent(String partialText);

    void deleteAll();
}
